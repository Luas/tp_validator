angular
	.module('app', ['ngMessages'])
	.controller('AppController', function AppController ($http) {
		var appController = this;

		appController.data = {
			"first_name": "",
			"last_name": "",
			"email": "",
			"password": "",
			"happy": false,
			"price": 8
		};
	})
	.directive('asyncEmail', function($q, $timeout) {
		return {
			// A besoin de ng-model sur le même noeud DOM
			require: 'ngModel',
			link: function (scope, element, attrs, ctrl) {
				ctrl.$asyncValidators.email = function (modelValue, viewValue) {
					if (ctrl.$isEmpty(modelValue)) {
						return $q.when();
					}

					var deferred = $q.defer();

					// Simule une réponse serveur
					$timeout(function() {
						var mails = ['toto@gmail.com', 'alex@gmail.com'];
						if (mails.indexOf(viewValue) == -1) {
							deferred.resolve();
						} else {
							deferred.reject();
						}
					}, 2000);

					return deferred.promise;
				};

			}
		}
	})
	.directive('pwdValidator',function(){
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ctrl){
				ctrl.$validators.oneUppercase = function(modelValue, viewValue){
					value = modelValue;
					for(var i = 0; i<value.length; i++){
						codeChar = value.charCodeAt(i);
						if(codeChar>=65 && codeChar<=90){
							return true;
						}
					}
					return false;
				}
				ctrl.$validators.oneNumber = function(modelValue, viewValue){
					value = modelValue;
					for(var i = 0; i<value.length; i++){
						codeChar = value.charCodeAt(i);
						if(codeChar>=48 && codeChar<=57){
							return true;
						}
					}
					return false;
				}
			}
		}
	})
	.directive('price', function(){
		return {
			require: 'ngModel',
			link: function(scope, el, attrs, ctrl){
				ctrl.$formatters.push(function(value){
					return value + ' CHF';
				});
				ctrl.$parsers.push(function(value){
					return Number(value);
				});
			}
		}
	});